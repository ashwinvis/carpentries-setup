# carpentries-setup

Bash scripts to setup a minimal user and flash forward to an episode

- setup.sh: create user and setup directories
- login.sh: login as the user
- teardown.sh: deletes user and their directories

## Configuration

- vars.sh: user name and other shared variables
- `bashrc.sh`: Copied as `~/.bashrc` for the user

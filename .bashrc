# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# write bash history more often
PROMPT_COMMAND='history -a'

# HISTORY
# -------
# bash attempts to save all lines of a multiple-line command in the same
# history entry.
shopt -s cmdhist
# If set, and the cmdhist option is enabled, multi-line commands are saved to
# the history with embedded newlines rather than using semicolon separators
# where possible.
shopt -s lithist
# append to the history file, don't overwrite it
shopt -s histappend

# AUTOCOMPLETION
# --------------
# If set, and readline is being used, bash will attempt to perform hostname
# completion when a word containing a @ is being completed (see Completing under
# READLINE above).  This is enabled by default.
shopt -s hostcomplete
# minor errors in the spelling of a directory component in a cd command will be
# corrected.
shopt -s cdspell

# DISPLAY
# -------
# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

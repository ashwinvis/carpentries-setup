#!/bin/bash
set -xeuo pipefail
. ./vars.sh
sudo userdel --remove $user
ls /home
sudo systemctl stop sshd

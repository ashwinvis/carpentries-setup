git config --global user.name "Ashwin V. Mohanan"
git config --global user.email "ashwinvis@users.noreply.github.com"
git config --global core.autocrlf input
git config --global core.editor "nano -w"
git config --global init.defaultBranch main
git config --list

set -xe

cd ~/Desktop
mkdir .04-tracking-changes
git clone .03*/planets .04-tracking-changes/planets
cd .04-tracking-changes/planets
cat > mars.txt <<EOF
Cold and dry, but everything is my favorite color
EOF
ls
cat mars.txt
git status
git add mars.txt
git status
git commit -m "Start notes on Mars as a base"
git status
git log
# Where are my changes
cat >> mars.txt <<EOF
The two moons may be a problem for Wolfman
EOF
git status
git diff
# git commit -m "Add concerns about effects of Mars' moons on Wolfman"
# Whoops
git add mars.txt
git commit -m "Add concerns about effects of Mars' moons on Wolfman"
# Staging area
cat >> mars.txt <<EOF
But the Mummy will appreciate the lack of humidity
EOF
git diff
git add mars.txt
git diff
git diff --staged
git commit -m "Discuss concerns about Mars' climate for Mummy"
git status
git log
git log -1
git log --oneline
git log --oneline --graph
# Directories
mkdir spaceships
git status
# git add spaceships
# Whoops
git status
touch spaceships/apollo-11 spaceships/sputnik-1
git status
git add spaceships
git status
git commit -m "Add some initial thoughts on spaceships"

# Commiting Multiple Fiels
cat > mars.txt <<EOF
Maybe I should start with a base on Venus.
EOF
cat > venus.txt <<EOF
Venus is a nice planet and I definitely should consider it as a base.
EOF
git add mars.txt venus.txt
git commit -m "Write plans to start a base on Venus"

cd ..
mkdir bio
cd bio
git init
cat > me.txt <<EOF
Hi! My name is ashwinvis
I use git
I teach git
EOF
git add me.txt
git commit -m "Add biography file"
# Modify one line
sed -i 's/ashwinvis/Ashwin/g' me.txt
# Add a fourth line
cat >> me.txt <<EOF
I am still learning git
EOF
git diff me.txt

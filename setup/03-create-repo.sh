set -xe

cd ~/Desktop
mkdir -p .03-create-repo/planets
ln -s .03-create-repo/planets planets
cd .03-create-repo/planets
git init
ls -a
git checkout -b main
git status

#!/bin/bash
set -xeuo pipefail
. ./vars.sh
sudo systemctl start sshd
sudo useradd --create-home --groups users --shell /bin/bash $user
sudo passwd $user
rsync -aP setup .bashrc .tmux.conf $user@localhost:
ssh $user@localhost 'bash -s' << EOF
  set -xe
  cd ~/setup
  bash 02-setup-git.sh
  bash 03-create-repo.sh
  bash 04-tracking-changes.sh
EOF
